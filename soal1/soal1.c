#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <sys/wait.h>
#include <dirent.h>

int status;

void _exec(char *cmd, char *argv[])
{
  pid_t child_id;
  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    execv(cmd, argv);
  }

  while ((wait(&status)) > 0);
}

void _mkdir(char *location)
{
  char *argv[] = {"mkdir", location, NULL};
  _exec("/bin/mkdir", argv);
}

void unzip(char *from, char *to)
{
  char *argv[] = {"unzip", from, "-d", to, NULL};
   _exec("/bin/unzip", argv);
}

int main() {
  _mkdir("./music");
  _mkdir("./quote");
  unzip("./music.zip", "./music");
  unzip("./quote.zip", "./quote");
  return 0;
}
