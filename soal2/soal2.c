#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <sys/wait.h>
#include <dirent.h>

int status;

void _exec(char *cmd, char *argv[])
{
  pid_t child_id;
  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    execv(cmd, argv);
  }

  while ((wait(&status)) > 0);
}

void _create_users_file() {
  char *argv[] = {"touch", "./users.txt", NULL};
  _exec("/usr/bin/touch", argv);
}

int ask() {
  int option;
  do {
    printf("1. register\n2. login\n0. quit\nanswer: ");
    scanf("%d", &option);
    if(option != 0 && option != 1 && option != 2) {
      printf("\ninput undefined!\n");
    }
    printf("\n");
  } while(option != 0 && option != 1 && option != 2);
  if(option == 0) exit(0);
  return option;
}

void _write_to_users(char *format) {
  FILE *fPointer;
  fPointer = fopen("users.txt", "a");
  fprintf(fPointer, format);
  fclose(fPointer);
}

char *_get_user_auth() {
  char username[255], pass[100];
  printf("masukkan username: ");
  scanf("%[^\n]%*c", username);
  do {
    printf("masukkan password (minimal 8 digit): ");
    scanf("%[^\n]%*c", pass);
    if(strlen(pass) < 8) {
      printf("\npassword minimal 8 karakter!\n\n");
    }
  } while(strlen(pass) < 8);
  printf("\n\n");
  char format[300];
  strcpy(format, username);
  strcat(format, ": ");
  strcat(format, pass);
  strcat(format, "\n");
  printf("%s\n", format);
  return format;
}

bool _search_user(char *user) {
  FILE *fPointer;
  fPointer = fopen("users.txt", "r");
  char singleLine[300];
  while(!feof(fPointer)) {
    fgets(singleLine, 300, fPointer);
    if(strcmp(singleLine, user) == 0) {
      fclose(fPointer);
      return 1;
    }
    puts(singleLine);
  }
  fclose(fPointer);
  return 0;
}

void _register() {
  char format[300];
  strcpy(format, _get_user_auth());
  _write_to_users(format);
}

void _login() {
  char format[300];
  strcpy(format, _get_user_auth());
  bool res = _search_user(format);
  if(res) printf("user found!");
  else printf("user not found!");
}

int main () {
  _create_users_file();
  int option = ask();
  if(option == 1) {
    _register();
  } else {
    _login();
  }
  return 0;
}