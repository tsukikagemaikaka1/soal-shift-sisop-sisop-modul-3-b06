# SOAL SHIFT MODUL 3 B06 2022

## ANGGOTA KELOMPOK

- Hesekiel Nainggolan (5025201054)
- Rycahaya Sri Hutomo (5025201046)
- Yehezkiel Wiradhika (5025201086)

## SOAL 1

### PENJELASAN SOAL

1. Kita disuruh untuk mendownload dua file zip
   unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. (Unzip ini dilakukan dengan bersamaan menggunakan thread)
2. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
3. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
   Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
4. Karena ada yang kurang, kita diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
   <br>
   <br>
   <strong>Catatan:</strong>

- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
  Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka.
- Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau perlu, di Modul 1 dan 2

### PENJELASAN JAWABAN
Hal pertama itu menuliskan library yang diperlukan, kemudian membuat program untuk membuat folder, dimana pertama kita butuh program exec untuk menampung <code> pid_t child_id </code>,
<code>void _exec(char *cmd, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  if (child_id < 0)
  { 
  exit(EXIT_FAILURE); 
  }
  if (child_id == 0)
  {
  execv(cmd, argv);
  }
  while ((wait(&status)) > 0);
}</code>

Kemudian, akan dibuat program untuk membuat folder
<br>
<code> void _mkdir(char *location)
{
  char *argv[] = {"mkdir", location, NULL};
  _exec("/bin/mkdir", argv);
}
</code>
<br>
Kemudian membuat program untuk mengunzip file yang sudah didownload tadi,
<code> void unzip(char *from, char *to)
{
  char *argv[] = {"unzip", from, "-d", to, NULL};
   _exec("/bin/unzip", argv);
} </code>


## SOAL 2

### PENJELASAN SOAL

A. Login dan Register

1. Kita diminta untuk membuat online judge sederhana.Online judge sederhana akan dibuat dengan sistem client-server
2. Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login.
3. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username: password.
4. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client.
5. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem.
6. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
  <br><br>
  Format users.txt:
  <br>
  Username:password
  <br>
  username2:password2

B. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge.

- Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan <code>\t</code>.
- File otomatis dibuat saat server dijalankan.

C. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:

- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

### PENJELASAN JAWABAN
Hal pertama ialah membuat library yang dibutuhkan, kemudian akan dibuat program untuk memilih register atau login:
<br><code> int ask() {
  int option;
  do {
    printf("1. register\n2. login\n0. quit\nanswer: ");
    scanf("%d", &option);
    if(option != 0 && option != 1 && option != 2) {
      printf("\ninput undefined!\n");
    }
    printf("\n");
  } while(option != 0 && option != 1 && option != 2);
  if(option == 0) exit(0);
  Return option;
} </code></br>

Jika yang dipilih adalah register, maka akan masuk kedalam program register:
<br><code>void _register() {
  char format[300];
  strcpy(format, _get_user_auth());
  _write_to_users(format);} </code></br>

Dimana nanti akan diarahkan untuk membuat username dan juga password:
<br><code>char *_get_user_auth() {
  char username[255], pass[100];
  printf("masukkan username: ");
  scanf("%[^\n]%*c", username);
  do { 
     printf("masukkan password (minimal 8 digit): ");
     scanf("%[^\n]%*c", pass);
     if(strlen(pass) < 8){ 
      printf("\npassword minimal 8 karakter!\n\n");}
  } while(strlen(pass) < 8);
  printf("\n\n");
  char format[300];
  strcpy(format, username);
  strcat(format, ": ");
  strcat(format, pass);
  strcat(format, "\n");
  printf("%s\n", format);
  return format;}</code></br>

Setelah dibuat username dan passwordnya, maka akan disimpan kedalam file user.txt:
<br><code>void _write_to_users(char *format) {
  FILE *fPointer;
  fPointer = fopen("users.txt", "a");
  fprintf(fPointer, format);
  fclose(fPointer);}</code></br>


Namun ,jika yang dipilih adalah login, maka akan masuk kedalam program login:
<br><code>void _login() {
  char format[300];
  strcpy(format, _get_user_auth());
  bool res = _search_user(format);
  if(res) printf("user found!");
  else printf("user not found!");}</code></br>

Dimana akan dicari username dan password yang disimpan dalam file user.txt
<br><code>bool _search_user(char *user) {
  FILE *fPointer;
  fPointer = fopen("users.txt", "r");
  char singleLine[300];
  while(!feof(fPointer)) {
     fgets(singleLine, 300, fPointer);
     if(strcmp(singleLine, user) == 0) {
      fclose(fPointer);
      return 1;}
     puts(singleLine);
  }
  fclose(fPointer);
  return 0;}</code>

## SOAL 3

### PENJELASAN SOAL

Pada soal ini, diketahui bahwa nami memiliki harta karun berdasarkan jenis/tipe/kategori/ekstensi harta karunnya, kemudian nami hendak mengirimkan harta karun tersebut ke kampung halamannya, kita diminta untuk :

1. Mengekstrak file zip yang diberikan ke dalam folder “/home/[user]/shift3/”, kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”.
2. Memastikan file semua di dalam sebuah folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
3. Setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.
4. Membuat program client-server dimana akan digunakan untuk mengirimkan file ke Cocoyasi Village, dimana file tersebut akan diekstrak terlebih dahulu ke dalam bentuk zip, dengan format nama “hartakarun.zip” ke working directory dari program client.
5. Membuat program yang mengirimkan file tersebut dengan menggunakan command send hartakarun.zip keserver
   <br>
   <br>
   <strong>Catatan:</strong>
   <br>
   - Kategori folder tidak dibuat secara manual, harus melalui program C
   - Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
   - Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder titik terdepan (contoh “tar.gz")
   - Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d
   

### PENJELASAN JAWABAN
